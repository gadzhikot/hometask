
public class Complex extends Pair {

	public Complex(int a, int b) {
		super(a, b);
	}
	public static void multiPair (Pair first,Pair second){
		double acBd = first.a1*second.b1-first.b1*second.b1;
		double adBc = first.a1*second.b1+second.b1*first.a1;
		System.out.println("("+first.a1+", "+first.b1+") * ("+second.a1+", "+second.b1+") = ("+acBd+", "+adBc+")" );
	}
	public static void diff(Pair first,Pair second){
		double ab = first.a1-first.b1;
		double cd = second.a1-second.b1;
		System.out.println("("+first.a1+" - "+first.b1+") , ("+second.a1+" - "+second.b1+") = ("+ab+", "+cd+")");
	}
}
