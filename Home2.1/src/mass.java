import java.io.*;
public class mass 
{
	static InputStreamReader rd= new InputStreamReader(System.in);
	static BufferedReader br = new BufferedReader(rd);
	
	public static void main(String[] args) throws NumberFormatException, IOException
	{
		int n;
		System.out.println("Введите длинну массива");
		n=Integer.parseInt(br.readLine());
		int mass[]=new int[n];
		System.out.println("Задайте массив");
		for (int i = 0;i<=n-1;i++)
		{
			mass[i]=Integer.parseInt(br.readLine());
		}
		System.out.print("По убыванию: ");
		for (int i=0;i<=n-1;i++)
		{
			for (int j=i+1;j<=n-1;j++)
			{
				if(mass[i]<mass[j])
				{
					   int buf=mass[i];
				       mass[i]=mass[j];
				       mass[j]=buf;					
				}	
			}
		}
		for (int i = 0;i<=n-1;i++)
		{
			System.out.print(mass[i]+"  ");
		}
		System.out.println();
		System.out.print("По возрастанию: ");
		for (int i=0;i<=n-1;i++)
		{
			for (int j=i+1;j<=n-1;j++)
			{
				if(mass[i]>mass[j])
				{
					   int buf=mass[i];
				       mass[i]=mass[j];
				       mass[j]=buf;					
				}	
			}
		}
		for (int i = 0;i<=n-1;i++)
		{
			System.out.print(mass[i]+"  ");
		}
	}
}
