import java.io.*;

public class Main 
{	
	static InputStreamReader rd= new InputStreamReader(System.in);
	static BufferedReader br = new BufferedReader(rd);
	private static float readFloat() throws NumberFormatException, IOException
	{
		return Float.parseFloat(br.readLine());
	}
	public static void main(String[] args) throws IOException
	{
		double d,a,b,c,x1,x2;
		System.out.println("Введите коэфиценты квадратного уравнения:");
		a = readFloat();
		b = readFloat();
		c = readFloat(); 
		if (a!=0 & b!=0 & c!=0)
		{
			d = Math.pow(b,2) - 4* a* c;
			System.out.println("Дскрименант = "+d);
			if (d<0)
				System.out.println("Уравнение не имеет решений");
			else
				if(d==0)
				{
					x1 = (-b)/(2*a);
					System.out.println("x = "+x1);
				}
				else
				{
					x1 = (((-b)+Math.sqrt(d))/(2*a));
					x2 = (float) (((-b)-Math.sqrt(d))/(2*a));
					System.out.println("x1 = "+x1);
					System.out.println("x2 = "+x2);
				}
		}
		else System.out.println("x может принимать любые значения");
	}
}
