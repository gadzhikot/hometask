import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class qSort {
	static InputStreamReader rd = new InputStreamReader(System.in);
	static BufferedReader br = new BufferedReader(rd);
	public static void qSort(int[] A, int low, int high) {
		int i = low;                
		int j = high;
		int x = A[(low+high)/2];  // x - опорный элемент посредине между low и high
		do 
		{
			while(A[i] < x) ++i;  // поиск элемента для переноса в старшую часть
			while(A[j] > x) --j;  // поиск элемента для переноса в младшую часть
			if(i <= j){           
				// обмен элементов местами:
				int temp = A[i];
				A[i] = A[j];
				A[j] = temp;
				// переход к следующим элементам:
				i++; j--;
			}
		}while(i < j);
		if(low < j) qSort(A, low, j);
		if(i < high) qSort(A, i, high);
	}
	//Ввод массива
public static void addArr(int arr[],int n) throws NumberFormatException, IOException
{
	for (int i=0;i<=n-1;i++)
	{
		Random r = new Random();
		arr[i] = Math.abs(r.nextInt(10));
	}
}
//Вывод массива
public static void writeArr(int arr[],int n)
{
	for (int i=0;i<=n-2;i++)
	{
		System.out.print(arr[i]+",");
	}
	System.out.println(arr[n-1]);
}
	


public static void main(String[] args) throws NumberFormatException, IOException
	{
		System.out.println("Введите размер массива");
		int n = Integer.parseInt(br.readLine());
		int[] mass = new int[n];
		addArr(mass,n);
		writeArr(mass,n);
		qSort(mass,0,n-1);
		writeArr(mass,n);
	}
}