
public class Pair {
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + a1;
		result = prime * result + b1;
		return result;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair other = (Pair) obj;
		if (a1 != other.a1)
			return false;
		if (b1 != other.b1)
			return false;
		return true;
	}
	//Конструктор класса
	public Pair(int a,int b){
		 this.a1=a;
		 this.b1=b;
		System.out.println("("+a1+", "+b1+")");
	}
		public Pair() {}
	
		protected int a1;
		protected int b1;
	//Умножение пары на число
	public void multiply(Pair para, int m){
		int a2=para.a1*m;
		int b2=para.b1*m;
		System.out.println("("+para.a1+", "+para.b1+") * "+m+" = ("+a2+", "+b2+")");
	}
	public void sum(Money para1,Money para2){
		int a2=para1.a1+para2.a1;
		int b2=para1.b1+para2.b1;
		System.out.println("("+para1.a1+", "+para1.b1+") + ("+para2.a1+", "+para2.b1+") = ("+a2+", "+b2+")");
	}
}
