
public class Money extends Pair{
	
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + kop;
		result = prime * result + rub;
		return result;
	}
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Money other = (Money) obj;
		if (kop != other.kop)
			return false;
		if (rub != other.rub)
			return false;
		return true;
	}
	public Money(int a, int b) {
		super(a, b);
	}
	public Money() {
	
	}
	int rub;
	int kop;
	public void moneyOperation(Money pair1, Money pair2,int multy,int diff){
		if (pair1.equals(pair2)){
			System.out.println("Пары равны");
		}
		sum(pair1,pair2);
		raznost(pair1,pair2);
		multiMoney(pair1,multy);
		multiMoney(pair2,multy);
		diffMoney(pair1, diff);
		diffMoney(pair2, diff);
		hashCodeOut(pair1,pair2);
	}
	public void kopToRub(){if (kop >= 100){
		rub = rub + (kop / 100);
		kop = kop % 100;
	}
	}
	public void sum(Money pair1,Money pair2){
		rub = pair1.a1+pair2.a1;
		kop = pair1.b1+pair2.b1;
		kopToRub();
		System.out.println(pair1.a1+" руб. "+pair1.b1+" коп. + "+pair2.a1+" руб. "+pair2.b1+" коп. = "+rub+" руб. "+kop+" коп.");
	}
	public void multiMoney(Money pair1,int multy){
		rub = pair1.a1*multy;
		kop = pair1.b1*multy;
		kopToRub();
		System.out.println(pair1.a1+" руб. "+pair1.b1+" коп. * "+multy+" = "+rub+" руб. "+kop+" коп.");
	}
	public void diffMoney(Money pair1,int diff){
		if(diff!=0){
		kop = pair1.b1 + pair1.a1 * 100;
		kop = kop / diff;
		rub=0;
		kopToRub();
		System.out.println(pair1.a1+" руб. "+pair1.b1+" коп. / "+diff+" = "+rub+" руб. "+kop+" коп.");
		}
		else System.out.println("На ноль делить нельзя");
	}
	public void raznost(Money pair1,Money pair2){

		System.out.print(pair1.a1+" руб. "+pair1.b1+" коп. - "+pair2.a1+" руб. "+pair2.b1+" коп. = ");
		rub = pair1.a1 - pair2.a1;
		kop = pair1.b1 - pair2.b1;
		if (rub>0 & kop<0){
			rub--;
			kop = 100+kop;
		}
		kopToRub();
		System.out.println(rub+" руб. "+kop+" коп.");
	}
	public void hashCodeOut(Money pair1, Money pair2){
	System.out.println("hashcode 1: "+pair1.hashCode());
	System.out.println("hashcode 2: "+pair2.hashCode());
		
	};
}