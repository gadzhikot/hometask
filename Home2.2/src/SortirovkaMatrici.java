import java.io.*;

public class SortirovkaMatrici 
{
	static InputStreamReader rd = new InputStreamReader(System.in);
	static BufferedReader br = new BufferedReader(rd);
	public static int readInt() throws NumberFormatException, IOException
	{
		return Integer.parseInt(br.readLine());
	}
	///////////////////////////////////////////////////////////
	public static void main(String[] args) throws NumberFormatException, IOException
	{
		//Введем размеры матриы
		System.out.println("Введите n и m");
		int n = readInt();
		int m = readInt();
		int mass[][] = new int[n][m];
		//Введем значения матрицы
		System.out.println("Введите значения массива");
		for (int i=0; i<=n-1;i++)
		{
			for(int j=0;j<=m-1;j++)
			{
				mass[i][j]=readInt();
			}
		}
		//Отсортируем матрицу
		for (int a=0;a<=n-1;a++)
		{
			for (int b=0;b<=m-1;b++)
			{
				for (int i=0;i<=n-1;i++)
				{
					for (int j=0;j<=m-1;j++)
					{
						if ((mass[i][j])>(mass[a][b]))
						{
							int buf=mass[a][b];
							mass[a][b]=mass[i][j];
							mass[i][j]=buf;
						}
					}
				}
			}
		}
		//Выводим отсортированную матрицу
		System.out.println("Отсортированный массив");
		for (int i=0; i<=n-1;i++)
		{
			for(int j=0;j<=m-1;j++)
			{
				System.out.println("n["+i+1+"]m["+j+1+"]="+mass[i][j]);
			}
		}
	}
}
